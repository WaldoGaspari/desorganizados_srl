Feature: Calendar with single events

  Background:
    Given employee "Juan Perez" exists
    And employee has scheduled "Correr tests" at "2019-08-23" from "10:00" to "11:00"

Scenario: Employee is available after the event
    Given we want to schedule a new event at "2019-08-23" from "11:00" to "12:00"
    When we ask for availability
    Then the response status is 200
    And the response message is "ok"
    And the employee is available  

  Scenario: Employee is available before the event
    Given we want to schedule a new event at "2019-08-23" from "9:00" to "10:00"
    When we ask for availability
    Then the response status is 200
    And the response message is "ok"
    And the employee is available

  Scenario: Employee is not available within the event
    Given we want to schedule a new event at "2019-08-23" from "10:30" to "11:00"
    When we ask for availability
    Then the response status is 200
    And the response message is "ok"
    And the employee is not available

  Scenario: Employee is not available starting in the middle and ending after the event
    Given we want to schedule a new event at "2019-08-23" from "10:30" to "12:00"
    When we ask for availability
    Then the response status is 200
    And the response message is "ok"
    And the employee is not available

  Scenario: Employee is not available starting before and ending in the middle of the event
    Given we want to schedule a new event at "2019-08-23" from "9:00" to "10:30"
    When we ask for availability
    Then the response status is 200
    And the response message is "ok"
    And the employee is not available

  Scenario: Employee is not available starting before and ending after the event
    Given we want to schedule a new event at "2019-08-23" from "9:00" to "12:00"
    When we ask for availability
    Then the response status is 200
    And the response message is "ok"
    And the employee is not available


  

