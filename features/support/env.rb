require_relative '../../app'
require 'rack/test'
require 'rspec/expectations'
require 'sinatra'
require 'sinatra/json' 

include Rack::Test::Methods

def app
	Sinatra::Application
end
