Feature: Find the first available slot in a calendar with several events

  Background:
    Given employee "Juan Perez" exists
    And employee has scheduled "Correr tests" at "2019-08-23" from "9:00" to "10:00"
    And employee has scheduled "Correr tests" at "2019-08-23" from "11:00" to "12:00" every 7 days until "2019-10-23"

  Scenario: Employee first available slot is before the first event
    Given we want to schedule a new event of 1 hour between "8:00" and "14:00" from "2019-08-23" until "2019-10-23"
    When we ask for the first available slot
    Then the response status is 200
    And the response message is "ok"
    And the slot is "2019-08-23" from "08:00" to "09:00"

  Scenario: Employee first available slot is between the first and second event
    Given we want to schedule a new event of 1 hour between "9:00" and "14:00" from "2019-08-23" until "2019-10-23"
    When we ask for the first available slot
    Then the response status is 200
    And the response message is "ok"
    And the slot is "2019-08-23" from "10:00" to "11:00"

  Scenario: Employee first available slot is after the first and before the second event
    Given we want to schedule a new event of 1 hour between "11:00" and "14:00" from "2019-08-23" until "2019-10-23"
    When we ask for the first available slot
    Then the response status is 200
    And the response message is "ok"
    And the slot is "2019-08-23" from "12:00" to "13:00"

  Scenario: Employee does not have a free slot
    Given we want to schedule a new event of 2 hour between "9:00" and "12:00" from "2019-08-23" until "2019-08-23"
    When we ask for the first available slot
    Then the response status is 404
    And the response message is "no available slot"
