Feature: Add calendar event

  Background:
    Given employee "Juan Perez" exists

  Scenario: Add valid event
    Given employee schedules "Correr tests" at "2019-08-23" from "10:00" to "11:00"
    When we submit the event for the employee
    Then the response status is 200
    And the response message is "ok"
  

  Scenario: Add calendar event with invalid date
    Given employee schedules "Correr tests" at "2019-08-xx" from "10:00" to "11:00"
    When we submit the event for the employee
    Then the response status is 400
    And the response message is "Invalid date/time."

  Scenario: Add calendar event with invalid description
    Given employee schedules "" at "2019-08-23" from "10:00" to "11:00"
    When we submit the event for the employee
    Then the response status is 400
    And the response message is "Invalid description."

  Scenario: Add calendar event with start time after end time
    Given employee schedules "Correr tests" at "2019-08-23" from "11:00" to "10:00"
    When we submit the event for the employee
    Then the response status is 400
    And the response message is "Invalid start/end time."