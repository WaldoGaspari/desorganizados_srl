Feature: Add employees

  Scenario: Add valid employee
    When we submit the employee "Juan Perez"
    Then the response status is 200
    And the response message is "ok"
    And the employee id is 1

  Scenario: Add invalid employee
    When we submit the employee ""
    Then the response status is 400
    And the response message is "Invalid name."

