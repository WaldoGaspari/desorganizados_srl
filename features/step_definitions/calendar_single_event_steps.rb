require_relative '../support/env'

Given(/^employee "([^"]*)" exists$/) do |nombre|
  post '/empleado', :nombre => nombre
end

Given(/^employee has scheduled "([^"]*)" at "([^"]*)" from "([^"]*)" to "([^"]*)"$/) do |nombre_evento, fecha, hora_inicio, hora_fin|
  post '/evento', :hora_inicio => hora_inicio, :hora_fin => hora_fin, :fecha => fecha, :nombre_evento => nombre_evento
end

Given(/^we want to schedule a new event at "([^"]*)" from "([^"]*)" to "([^"]*)"$/) do |fecha, hora_inicio, hora_fin|
  @fecha_evento = fecha
  @hora_inicio_evento = hora_inicio
  @hora_fin_evento = hora_fin
end

When(/^we ask for availability$/) do
  get '/disponibilidad_evento', :hora_inicio => @hora_inicio_evento, :hora_fin => @hora_fin_evento, :fecha => @fecha_evento
end

Then(/^the employee is available$/) do
  expect(last_response.body).to eq "Hay disponibilidad para el evento."
end

Then(/^the employee is not available$/) do
 expect(last_response.body).to eq "No hay disponibilidad para el evento." 
end
