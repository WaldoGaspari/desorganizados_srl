require_relative '../support/env'

Given(/^employee schedules "([^"]*)" at "([^"]*)" from "([^"]*)" to "([^"]*)"$/) do |nombre_evento, fecha_evento, hora_inicio, hora_fin|
  @nombre_evento = nombre_evento
  @fecha_evento = fecha_evento
  @hora_inicio = hora_inicio
  @hora_fin = hora_fin
end

When(/^we submit the event for the employee$/) do
  post '/evento', :hora_inicio => @hora_inicio, :hora_fin => @hora_fin, :fecha => @fecha_evento, :nombre_evento => @nombre_evento
end

