require_relative '../support/env'

Given(/^we want to schedule a new event of (\d+) hour between "([^"]*)" and "([^"]*)" from "([^"]*)" until "([^"]*)"$/) do |duracion, rango_hora_inicio, rango_hora_fin, fecha_inicio, fecha_fin|
  @duracion = duracion
  @rango_hora_inicio = rango_hora_inicio
  @rango_hora_fin = rango_hora_fin
  @fecha_inicio = fecha_inicio
  @fecha_fin = fecha_fin
end

When(/^we ask for the first available slot$/) do
  get '/disponibilidad_primer_evento', :hora_comienzo => @rango_hora_inicio, :hora_fin => @rango_hora_fin, :duracion => @duracion, :fecha_inicio => @fecha_inicio, :fecha_fin => @fecha_fin
end

Then(/^the slot is "([^"]*)" from "([^"]*)" to "([^"]*)"$/) do |fecha, hora_inicio, hora_fin|
  last_response.body.include? fecha
  last_response.body.include? hora_inicio
  last_response.body.include? hora_fin
end
