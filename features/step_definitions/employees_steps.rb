require_relative '../support/env'

When(/^we submit the employee "([^"]*)"$/) do |nombre|
  post '/empleado', :nombre => nombre
end

Then(/^the response status is (\d+)$/) do |status|
  expect(last_response.status).to eq(status.to_i)
end

Then(/^the response message is "([^"]*)"$/) do |message|
  last_response.body.include? message
end

Then(/^the employee id is (\d+)$/) do |id|

end
