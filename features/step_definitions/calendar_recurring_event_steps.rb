require_relative '../support/env'

Given(/^employee has scheduled "([^"]*)" at "([^"]*)" from "([^"]*)" to "([^"]*)" every (\d+) days until "([^"]*)"$/) do |nombre, fecha, hora_inicio, hora_fin, dias, fecha_fin|
  post '/evento_repetido', :hora_inicio => hora_inicio, :hora_fin => hora_fin, :fecha => fecha, :nombre_evento => nombre, :dias => dias, :fecha_fin => fecha_fin
end
