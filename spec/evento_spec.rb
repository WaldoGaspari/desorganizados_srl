require 'rspec'
require_relative '../model/evento'

describe 'Evento' do
   
  it 'se puede crear un evento con hora inicio 10:00, hora fin 11:00, fecha 2019-12-01 y nombre "Reunion de consorcio"' do
    @evento = Evento.new("10:00", "11:00", "2019-12-01", "Reunion de consorcio")
    expect(@evento.obtener_fecha()).to eq "2019-12-01"
    expect(@evento.obtener_hora_inicio()).to eq "10:00"
    expect(@evento.obtener_hora_fin()).to eq "11:00"
    expect(@evento.obtener_nombre()).to eq "Reunion de consorcio"
  end

  it 'no se puede crear un evento con una duracion de 10 horas' do
    expect{ Evento.new("10:00", "20:00", "2019-12-01", "Congreso") }.to raise_error StandardError, "Duración del evento invalido. Tiene que ser menor a 9 horas y mayor a 30 minutos."
  end

  it 'no se puede crear un evento con una duracion de 20 minutos' do
    expect{ Evento.new("10:00", "10:20", "2019-12-01", "Charla con empleados") }.to raise_error StandardError, "Duración del evento invalido. Tiene que ser menor a 9 horas y mayor a 30 minutos."
  end

  it 'no se puede crear un evento sin un nombre' do
    expect{ Evento.new("10:00", "11:20", "2019-12-01", "") }.to raise_error StandardError, "Invalid description."
  end

  it 'la hora de inicio no puede terminar distinto a en punto o y media' do
    expect{ Evento.new("10:20", "11:00", "2019-12-01", "Reunion importante") }.to raise_error StandardError, "Hora de inicio inválida. Tiene que terminar en punto (:00) o y media (:30)."
  end

  it 'la hora de fin no puede terminar distinto a en punto o y media' do
    expect{ Evento.new("10:00", "11:10", "2019-12-01", "Reunion importante") }.to raise_error StandardError, "Hora de fin inválida. Tiene que terminar en punto (:00) o y media (:30)."
  end

  it 'la hora de inicio no puede ser mayor a la hora de fin' do
    expect{ Evento.new("11:00", "10:00", "2019-12-01", "Reunion importante") }.to raise_error StandardError, "Invalid start/end time."
  end

  it 'si se pone una fecha erronea se lanza una excepcion' do
    expect{ Evento.new("10:00", "11:00", "2019-12-50", "Reunion importante") }.to raise_error RuntimeError, "Invalid date/time."
  end

  it 'si se pone una hora de inicio erronea se lanza una excepcion' do
    expect{ Evento.new("30:00", "11:00", "2018-12-15", "Reunion importante") }.to raise_error RuntimeError, "Invalid date/time."
  end

  it 'si se pone una hora de fin erronea se lanza una excepcion' do
    expect{ Evento.new("12:00", "44:00", "2018-12-15", "Reunion importante") }.to raise_error RuntimeError, "Invalid date/time."
  end

end