require 'rspec'
require 'date' 
require_relative '../model/calendario'

describe 'Calendario' do
   
  let(:calendario) { Calendario.new }
  
  it 'se puede crear un calendario sin evento alguno' do
    expect(calendario.obtener_cantidad_de_eventos()).to eq 0
  end

  it 'se puede agregar un evento al calendario' do
    calendario.agregar_evento_unico(Evento.new("10:00", "11:00", "2019-12-01", "Reunion de consorcio"))
    expect(calendario.obtener_cantidad_de_eventos()).to eq 1
  end

  it 'se puede agregar un evento repetido cada 14 dias desde el 03-05-2019 hasta el 05-08-2019' do
    calendario.agregar_evento_repetido((Evento.new("09:00", "10:30", "2019-05-03", "Reunion de consorcio")), 14, "2019-08-05")
    expect(calendario.obtener_cantidad_de_eventos()).to eq 7
  end

  it 'se puede agregar un evento repetido cada 7 dias desde el 02-02-2019 hasta el 12-05-2019' do
    calendario.agregar_evento_repetido((Evento.new("10:00", "11:30", "2019-02-02", "Congreso")), 7, "2019-05-12")
    expect(calendario.obtener_cantidad_de_eventos()).to eq 15
  end

  it 'se encuentra lugar disponible despues dada una fecha exacta y un rango de horario definido' do
    calendario.agregar_evento_unico(Evento.new("10:00", "11:00", "2019-07-04", "Congreso"))
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("11:00", "12:00", "2019-07-04")).to be_truthy
  end

  it 'se encuentra lugar disponible antes dada una fecha exacta y un rango de horario definido' do
    calendario.agregar_evento_unico(Evento.new("10:00", "11:00", "2019-07-04", "Congreso"))
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("09:00", "10:00", "2019-07-04")).to be_truthy
  end

  it 'no se encuentra lugar disponible para una fecha exacta y un rango de horario definido' do
    calendario.agregar_evento_unico(Evento.new("10:00", "11:00", "2019-07-04", "Congreso"))
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("10:30", "11:00", "2019-07-04")).to be_falsey
  end

  it 'no se encuentra lugar disponible para un evento en el cual tiene ya un evento en el medio' do
    calendario.agregar_evento_unico(Evento.new("10:00", "11:00", "2019-07-04", "Congreso"))
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("09:00", "12:00", "2019-07-04")).to be_falsey
  end

  it 'no se encuentra lugar disponible para un evento en el cual todavia hay otro evento transcurriendo' do
    calendario.agregar_evento_unico(Evento.new("10:00", "11:00", "2019-07-04", "Congreso"))
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("09:00", "10:30", "2019-07-04")).to be_falsey
  end

  it 'no se encuentra lugar disponible para un evento en el cual ya comenzo un evento' do
    calendario.agregar_evento_unico(Evento.new("10:00", "11:00", "2019-07-04", "Congreso"))
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("10:30", "12:00", "2019-07-04")).to be_falsey
  end

  it 'no se encuentra lugar disponible para una fecha exacta y un rango de horario definido con 2 eventos' do
    calendario.agregar_evento_unico(Evento.new("09:00", "10:00", "2019-07-04", "Congreso"))
    calendario.agregar_evento_unico(Evento.new("10:00", "11:00", "2019-07-04", "Reunion importante"))
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("08:00", "12:00", "2019-07-04")).to be_falsey
  end

  it 'se encuentra lugar disponible para un evento dentro de un conjunto de eventos recurrentes.' do
    calendario.agregar_evento_repetido((Evento.new("10:00", "11:00", "2019-08-23", "Correr tests")), 7, "2019-10-23")
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("11:00", "12:00", "2019-08-23")).to be_truthy
  end

  it 'se encuentra lugar disponible para un evento dentro de un conjunto de eventos recurrentes.' do
    calendario.agregar_evento_repetido((Evento.new("10:00", "11:00", "2019-08-23", "Correr tests")), 7, "2019-10-23")
    expect(calendario.encontrar_lugar_disponible_en_fecha_exacta("09:00", "12:00", "2019-08-23")).to be_falsey
  end

  it 'se encuentra el primer lugar disponible de 09:00 a 10:00 del dia 01-01 para el rango de fechas 01-01-19 al 07-01-19' do
    calendario.agregar_evento_unico(Evento.new("08:00", "09:00", "2019-01-01", "Reunion"))
    calendario.agregar_evento_unico(Evento.new("11:00", "11:30", "2019-01-01", "Reunion"))
    calendario.agregar_evento_unico(Evento.new("14:00", "15:00", "2019-01-02", "Reunion"))
    calendario.agregar_evento_unico(Evento.new("17:00", "19:00", "2019-01-03", "Reunion"))
    evento_disponible = calendario.encontrar_lugar_disponible_en_rango_fechas("09:00", "12:00", 1, "2019-01-01", "2019-01-07") 
    expect(evento_disponible.obtener_fecha()).to eq "2019-01-01"
  end

  it 'se encuentra el primer lugar disponible de 16:00 a 16:30 del dia 10-03 para el rango de fechas 10-03-19 al 20-03-19' do
    calendario.agregar_evento_unico(Evento.new("15:00", "15:30", "2019-03-10", "Reunion"))
    calendario.agregar_evento_unico(Evento.new("15:30", "16:00", "2019-03-10", "Reunion 2"))
    calendario.agregar_evento_unico(Evento.new("16:30", "17:00", "2019-03-10", "Reunion 3"))
    calendario.agregar_evento_unico(Evento.new("15:00", "17:00", "2019-03-11", "Reunion"))
    calendario.agregar_evento_unico(Evento.new("09:30", "10:30", "2019-03-15", "Reunion"))
    evento_disponible = calendario.encontrar_lugar_disponible_en_rango_fechas("15:00", "17:00", 0.5, "2019-03-10", "2019-03-20") 
    expect(evento_disponible.obtener_fecha()).to eq "2019-03-10"
  end

  it 'se encuentra el primer lugar disponible de 08:00 a 09:00 del dia 23-08 para el rango de fechas 23-08-19 al 23-10-19' do
    calendario.agregar_evento_unico(Evento.new("09:00", "10:00", "2019-08-23", "Correr tests"))
    calendario.agregar_evento_repetido((Evento.new("11:00", "12:00", "2019-08-23", "Correr tests")), 7, "2019-10-23")
    evento_disponible =calendario.encontrar_lugar_disponible_en_rango_fechas("08:00", "14:00", 1, "2019-08-23", "2019-10-23")
    expect(evento_disponible.obtener_fecha()).to eq "2019-08-23"
  end

  it 'se encuentra el primer lugar disponible de 10:00 a 11:00 del dia 23-08 para el rango de fechas 23-08-19 al 23-10-19' do
    calendario.agregar_evento_unico(Evento.new("09:00", "10:00", "2019-08-23", "Correr tests"))
    calendario.agregar_evento_repetido((Evento.new("11:00", "12:00", "2019-08-23", "Correr tests")), 7, "2019-10-23")
    evento_disponible = calendario.encontrar_lugar_disponible_en_rango_fechas("09:00", "14:00", 1, "2019-08-23", "2019-10-23") 
    expect(evento_disponible.obtener_fecha()).to eq "2019-08-23"
  end

  it 'se encuentra el primer lugar disponible de 12:00 a 13:00 del dia 23-08 para el rango de fechas 23-08-19 al 23-10-19' do
    calendario.agregar_evento_unico(Evento.new("09:00", "10:00", "2019-08-23", "Correr tests"))
    calendario.agregar_evento_repetido((Evento.new("11:00", "12:00", "2019-08-23", "Correr tests")), 7, "2019-10-23")
    evento_disponible = calendario.encontrar_lugar_disponible_en_rango_fechas("11:00", "14:00", 1, "2019-08-23", "2019-10-23") 
    expect(evento_disponible.obtener_fecha()).to eq "2019-08-23"
  end

  it 'no se encuentra lugar disponible para el rango de fechas 23-08-19 al 23-08-19' do
    calendario.agregar_evento_unico(Evento.new("09:00", "10:00", "2019-08-23", "Correr tests"))
    calendario.agregar_evento_repetido((Evento.new("11:00", "12:00", "2019-08-23", "Correr tests")), 7, "2019-10-23")
    expect(calendario.encontrar_lugar_disponible_en_rango_fechas("09:00", "12:00", 2, "2019-08-23", "2019-08-23")).to eq nil
  end

end