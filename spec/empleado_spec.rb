require 'rspec' 
require_relative '../model/empleado'

describe 'Empleado' do
   
  it 'se puede crear un empleado con el nombre "Juan Perez"' do
    expect(Empleado.new("Juan Perez").obtener_nombre()).to eq "Juan Perez"
  end

  it 'se pueden crear dos empleados, uno con el nombre "Juan Perez" y el otro con el nombre "Lucia Galvan"' do
    expect(Empleado.new("Juan Perez").obtener_nombre()).to eq "Juan Perez"
    expect(Empleado.new("Lucia Galvan").obtener_nombre()).to eq "Lucia Galvan"
  end

  it 'no se puede crear un empleado sin un nombre asignado' do
    expect{ Empleado.new("") }.to raise_error StandardError, "Invalid name."
  end

end