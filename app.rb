require 'sinatra'
require 'sinatra/json' 
require_relative '../desorganizados_srl/model/empleado'
require_relative '../desorganizados_srl/model/evento'
require_relative '../desorganizados_srl/model/calendario'

empleados = Array.new()
calendario = Calendario.new()

post '/empleado' do
  begin
  	empleado = Empleado.new(params[:nombre])
    empleados.push(empleado)
    status 200
    body "ok"
  rescue StandardError => error
    halt 400, body { error.message }
  end 
end

post '/evento' do
  begin
    calendario.agregar_evento_unico(Evento.new(params[:hora_inicio], params[:hora_fin], params[:fecha], params[:nombre_evento]))
    status 200
    body "ok"
  rescue StandardError => error
    halt 400, body { error.message }
  end
end

post '/evento_repetido' do
  begin
    calendario.agregar_evento_repetido((Evento.new(params[:hora_inicio], params[:hora_fin], params[:fecha], params[:nombre_evento])), params[:dias], params[:fecha_fin])
    status 200
    body "ok"
  rescue StandardError => error
    halt 400, body { error.message }
  end
end

get '/disponibilidad_evento' do
  begin
    resultado = calendario.encontrar_lugar_disponible_en_fecha_exacta(params[:hora_inicio], params[:hora_fin], params[:fecha])
    status 200
    body "ok"
    if(resultado == true)
      body "Hay disponibilidad para el evento."
    else
      body "No hay disponibilidad para el evento."
    end
  rescue StandardError => error
    halt 400, body { error.message }
  end
end

get '/disponibilidad_primer_evento' do
    resultado = calendario.encontrar_lugar_disponible_en_rango_fechas(params[:hora_comienzo], params[:hora_fin], params[:duracion].to_i, params[:fecha_inicio], params[:fecha_fin])
    if(resultado != nil)
      status 200
      body "ok"
      body { resultado.obtener_fecha(); resultado.obtener_hora_inicio(); resultado.obtener_hora_fin() }
    else
      status 404
      body { "no available slot" }
    end
end