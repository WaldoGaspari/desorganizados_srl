class DuracionEventoException < StandardError

  def initialize
    raise "Duración del evento invalido. Tiene que ser menor a 9 horas y mayor a 30 minutos."
  end
end