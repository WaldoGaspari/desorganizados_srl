require 'date'
require 'time'
require_relative '../model/duracion_evento_exception'
require_relative '../model/duracion_evento_exception'
require_relative '../model/nombre_evento_exception'
require_relative '../model/hora_inicio_exception'
require_relative '../model/hora_fin_exception'

HORA_MAXIMA = 9
HORA_MINIMA = 0.3

class ValidadorDeEventos

  def analizar_parametros(hora_inicio, hora_fin, fecha, nombre)
  	analizar_fecha_y_hora(fecha, hora_inicio, hora_fin)
  	@hora_inicio = hora_inicio.sub(":", ".")
  	@hora_fin = hora_fin.sub(":", ".")
  	analizar_hora_inicio_fin(@hora_inicio, @hora_fin)
  	analizar_duracion_evento(@hora_inicio, @hora_fin)
  	analizar_nombre_evento(nombre)
  	analizar_terminacion_hora_inicio_y_fin(@hora_inicio, @hora_fin)
  end

  def analizar_duracion_evento(hora_inicio, hora_fin)
  	if((hora_fin.to_f - hora_inicio.to_f) > HORA_MAXIMA || (hora_fin.to_f - hora_inicio.to_f) < HORA_MINIMA)
      DuracionEventoException.new
  	end
  end

  def analizar_nombre_evento(nombre)
    if(nombre == "")
      NombreEventoException.new
    end
  end

  def analizar_terminacion_hora_inicio_y_fin(hora_inicio, hora_fin)
    if(!hora_inicio.end_with?("00") && !hora_inicio.end_with?("30"))
      HoraInicioException.new
    end

    if(!hora_fin.end_with?("00") && !hora_fin.end_with?("30"))
      HoraFinException.new
    end
  end

  def analizar_hora_inicio_fin(hora_inicio, hora_fin)
  	if(hora_inicio.to_f > hora_fin.to_f)
      raise "Invalid start/end time."
    end
  end

  def analizar_fecha_y_hora(fecha, hora_inicio, hora_fin)
  	begin
      @fecha = Date.parse(fecha)
      @hora_inicio = Time.parse(hora_inicio)
      @hora_fin = Time.parse(hora_fin)
    rescue ArgumentError
      raise "Invalid date/time."
    end
  end
	
end