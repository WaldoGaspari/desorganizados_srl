require_relative '../model/nombre_empleado_exception'

class Empleado
  attr_reader :nombre
	
  def initialize (nombre)
    if(nombre == "")
      NombreEmpleadoException.new
    else
  	@nombre = nombre
    end
  end

  def obtener_nombre
  	@nombre
  end

end