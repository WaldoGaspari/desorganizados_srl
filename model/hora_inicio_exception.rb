class HoraInicioException < StandardError

  def initialize
    raise "Hora de inicio inválida. Tiene que terminar en punto (:00) o y media (:30)."
  end
end