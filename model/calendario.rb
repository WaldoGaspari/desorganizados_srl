require_relative '../model/evento'

MINUTOS = 60
SEGUNDOS = 60
DIAS = 1

class Calendario
	
  def initialize
    @eventos = Array.new()
  end

  def obtener_cantidad_de_eventos
  	@eventos.length
  end

  def agregar_evento_unico(evento)
    @eventos.push(evento)
  end

  def agregar_evento_repetido(evento, dias, fecha_fin)
    @fecha_fin = Date.parse(fecha_fin)
    fecha_inicial = Date.parse(evento.obtener_fecha)
    while fecha_inicial < @fecha_fin
      agregar_evento_unico(Evento.new(evento.obtener_hora_inicio, evento.obtener_hora_fin, fecha_inicial.strftime("%F"), evento.obtener_nombre))
      fecha_inicial = fecha_inicial + dias.to_i
    end
  end

  def encontrar_lugar_disponible_en_fecha_exacta(hora_inicio, hora_fin, fecha)
    resultado = false
    @eventos_fecha = Array.new()
    for evento in @eventos do 
      if(evento.obtener_fecha == fecha)
        @eventos_fecha.push(evento)
      end
    end
    for evento_fecha in @eventos_fecha do
      if((Time.parse(hora_inicio) > Time.parse(evento_fecha.obtener_hora_inicio()) && Time.parse(hora_inicio) >= Time.parse(evento_fecha.obtener_hora_fin())) || (Time.parse(hora_inicio) < Time.parse(evento_fecha.obtener_hora_inicio()) && Time.parse(hora_fin) <= Time.parse(evento_fecha.obtener_hora_inicio())))
        resultado = true
      end
    end
    return resultado
  end

  def encontrar_lugar_disponible_en_rango_fechas(hora_comienzo, hora_fin, duracion, fecha_inicio, fecha_fin)
    @fecha_inicio = Date.parse(fecha_inicio)
    @fecha_fin = Date.parse(fecha_fin)
    @fecha = @fecha_inicio
    resultado = true
    hora_comienzo_busqueda = Time.parse(hora_comienzo)
    hora_fin_busqueda = hora_comienzo_busqueda + (duracion*MINUTOS*SEGUNDOS)
    while @fecha >= @fecha_inicio && @fecha <= @fecha_fin
      for evento in @eventos do
        if(evento.obtener_fecha == @fecha.strftime("%F"))
          if(Time.parse(evento.obtener_hora_inicio) >= hora_comienzo_busqueda && Time.parse(evento.obtener_hora_fin) <= hora_fin_busqueda)
            resultado = false
          end
        end
      end
      if (resultado == false)
        hora_comienzo_busqueda = hora_comienzo_busqueda + (duracion*MINUTOS*SEGUNDOS)
        hora_fin_busqueda = hora_comienzo_busqueda + (duracion*MINUTOS*SEGUNDOS)
        resultado = true
      else
        if(resultado == true)
          evento_disponible = Evento.new(hora_comienzo_busqueda.strftime("%R"), hora_fin_busqueda.strftime("%R"), @fecha.strftime("%F"), "Evento disponible")
          return evento_disponible
        else
          return evento_disponible = nil
        end
      end
      if(hora_comienzo_busqueda >= Time.parse(hora_fin))
        @fecha = @fecha + DIAS
        hora_comienzo_busqueda = Time.parse(hora_comienzo)
        hora_fin_busqueda = hora_comienzo_busqueda + (duracion*MINUTOS*SEGUNDOS)
        resultado = true
      end
    end
  end

end