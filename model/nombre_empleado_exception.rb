class NombreEmpleadoException < StandardError

  def initialize
    raise "Invalid name."
  end
end