require 'date'
require 'time'
require_relative '../model/validador_de_eventos'

class Evento
  attr_reader :nombre
	
  def initialize (hora_incio, hora_fin, fecha, nombre)
    @validador = ValidadorDeEventos.new
    @validador.analizar_parametros(hora_incio, hora_fin, fecha, nombre)
    @hora_incio = Time.parse(hora_incio)
    @hora_fin = Time.parse(hora_fin)
    @fecha = Date.parse(fecha)
    @nombre = nombre
  end

  def obtener_nombre
  	@nombre
  end

  def obtener_hora_inicio
    @hora_incio.strftime("%R")
  end

  def obtener_hora_fin
    @hora_fin.strftime("%R")
  end

  def obtener_fecha
    @fecha.strftime("%F")
  end

end